import os
import subprocess

docker_image = 'bitbucketpipelines/anchore-pipe-python:ci' + os.getenv('BITBUCKET_BUILD_NUMBER', 'local')

def docker_build():
  """
  Build the docker image for tests.
  :return:
  """
  args = [
    'docker',
    'build',
    '-t',
    docker_image,
    '.',
  ]
  subprocess.run(args, check=True)

def setup():
  # build the image to test
  docker_build()

  # get a simple image staged to test analysis
  os.system('docker pull alpine:latest')

def test_no_parameters():
  args = [
    'docker',
    'run',
    docker_image,
  ]

  result = subprocess.run(args, check=False, text=True, capture_output=True)
  assert result.returncode == 1
  assert '✖ Validation errors: \nIMAGE_NAME:\n- required field' in result.stdout

def test_success():
  args = [
    'docker',
    'container',
    'run',
    '-v', '/usr/local/bin/docker:/usr/local/bin/docker:ro',
    '-v', '/var/run/docker.sock:/var/run/docker.sock',
    '--add-host=host.docker.internal:{}'.format(os.getenv("BITBUCKET_DOCKER_HOST_INTERNAL")),
    '-e', 'IMAGE_NAME=alpine:latest',
    '-e', 'BITBUCKET_DOCKER_HOST_INTERNAL={}'.format(os.getenv("BITBUCKET_DOCKER_HOST_INTERNAL")),
    '-e', 'DOCKER_HOST=tcp://host.docker.internal:2375',
    docker_image,
  ]
  result = subprocess.run(args, check=False, text=True, capture_output=True)
  if result.returncode != 0:
    print ("STDOUT: {}".format(result.stdout))
    print ("STDERR: {}".format(result.stderr))   

  #assert 'hello world' in result.stdout
  assert result.returncode == 0

