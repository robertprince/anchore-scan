FROM python:3.7-slim
ARG INLINE_SCAN_HASH=a3e63649a4ae2eb57538f78a99814a2579df73129e2eb7d3e75cd7feb85dea11
ARG INLINE_SCAN_VERSION=v0.8.1

COPY requirements.txt /

RUN apt-get -y update && \
    apt-get -y install curl && \
    pip install -r /requirements.txt && \
    apt-get -y clean

RUN curl -fSL -o /inline_scan "https://ci-tools.anchore.io/inline_scan-${INLINE_SCAN_VERSION}" && \
    echo "$INLINE_SCAN_HASH *inline_scan" | sha256sum -c - && \
    chmod +x /inline_scan
    
COPY data /
COPY pipe /
COPY pipe.yml /

ENTRYPOINT ["python3", "/pipe.py"]
